import { defineConfig } from "vite";

export default defineConfig({
    // use relative base path to enable hosting this app on a subpath
    base: "./",
});