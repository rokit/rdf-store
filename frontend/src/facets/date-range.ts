import { customElement } from 'lit/decorators.js'
import 'mdui/components/button-icon'
import 'mdui/components/icon'
import 'mdui/components/range-slider'
import { NumberRangeFacet } from './number-range'

@customElement('date-range-facet')
export class DateRangeFacet extends NumberRangeFacet {

    constructor(indexField: string) {
        super(indexField)
        this.type = 'date'
        this.icon = 'date_range'
    }

    updated() {
        if (this.slider) {
            this.slider.labelFormatter = (value) => { return this.formatDate(value, true) }
        }
    }

    updateValues(aggs: Record<string, string>) {
        // convert dates to epoch milliseconds so that parent class can handle all the facet logic
        const numbered: Record<string, number> =  {}
        Object.entries(aggs).forEach((value) => {
            numbered[value[0]] = new Date(value[1]).getTime() / 1000
        })
        super.updateValues(numbered)
    }

    applyFilterQuery(query: string[]) {
        if (this.value?.length === 2) {
            query.push(`${this.indexField}:[${this.formatDate(this.value[0])} TO ${this.formatDate(this.value[1])}]`)
        }
    }

    formatDate(epoch: number, slice?: boolean): string {
        const date = new Date(0)
        date.setUTCSeconds(epoch)
        let result = date.toISOString()
        if (slice) {
            result = result.slice(0, 10)
        }
        return result
    }
}
