import { html, nothing } from 'lit'
import { RangeSlider } from 'mdui/components/range-slider'
import 'mdui/components/button-icon'
import 'mdui/components/icon'

import { Facet } from './base'
import { customElement, property, query } from 'lit/decorators.js'

@customElement('number-range-facet')
export class NumberRangeFacet extends Facet {
    @query('#slider')
    slider?: RangeSlider
    @property()
    min?: number
    @property()
    max?: number
    @property()
    value?: number[]
    lastSelectedValue?: number[]
    icon = 'pin'
    type = 'numeric'

    constructor(indexField: string) {
        super(indexField)
    }

    firstUpdated() {
        this.slider!.value = this.value || []
    }

    updateValues(aggs: Record<string, number | string>) {
        if (!this.valid || !this.active) {
            this.min = aggs[`${this.indexField}_min`] as number
            this.max = aggs[`${this.indexField}_max`] as number
        }
        this.valid = this.min !== undefined && isFinite(this.min) && this.max !== undefined && isFinite(this.max) && this.min <= this.max
        if (this.valid) {
            if (this.lastSelectedValue !== undefined) {
                this.value = [
                    (this.lastSelectedValue[0] < this.min!) ? this.min! : this.lastSelectedValue[0],
                    (this.lastSelectedValue[1] > this.max!) ? this.max! : this.lastSelectedValue[1]
                ]
            } else {
                this.value = [ this.min!, this.max! ]
            }
        }
        // first let the new value update pass into the DOM and then update slider value to prevent glitches
        setTimeout(() => {
            if (this.slider) {
                this.slider.value = this.value || []
            }
        })
        this.updateActive()
    }

    reset() {
        if (this.valid) {
            this.value = [this.min!, this.max!]
        } else {
            this.value = undefined
        }
        if (this.slider) {
            this.slider.value = this.value || []
        }
        this.lastSelectedValue = undefined
        this.active = false
        this.dispatchEvent(new Event('change', { bubbles: true }))
    }

    onChange() {
        this.value = this.slider!.value
        this.lastSelectedValue = [...this.value]
        this.updateActive()
        this.dispatchEvent(new Event('change', { bubbles: true }))
    }

    applyAggregationQuery(facets: Record<string, string>) {
        facets[`${this.indexField}_min`] = `min(${this.indexField})`
        facets[`${this.indexField}_max`] = `max(${this.indexField})`
    }

    applyFilterQuery(query: string[]) {
        if (this.value) {
            query.push(`${this.indexField}:[${this.value[0]} TO ${this.value[1]}]`)
        }
    }

    updateActive() {
        this.active = this.valid && (this.value !== undefined && (this.value[0] > this.min! || this.value[1] < this.max!))
    }

    render() {
        return this.valid ? html`
            <header title="Type: ${this.type}" class="${this.active ? 'active' : ''}">
                <mdui-icon>${this.icon}</mdui-icon>
                <span class="label">${this.label}</span>
                <mdui-button-icon icon="replay" class="reset-button" title="Reset" @click="${() => { this.reset() }}"></mdui-button-icon>
            </header>
            <mdui-range-slider id="slider" class="w-100" min="${this.min}" max="${this.max}" @change="${() => this.onChange()}"></mdui-range-slider>
        ` : nothing
    }
}
