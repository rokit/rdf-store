import { Literal } from 'n3'
import { PREFIX_DCTERMS, PREFIX_RDF, PREFIX_RDFS, PREFIX_SHACL } from '../constants'
import { search, Schema, Field } from '../solr'
import { Facet } from './base'
import { KeywordFacet } from './keyword'
import { GeoLocationFacet } from './geo-location'
import { NumberRangeFacet } from './number-range'
import { DateRangeFacet } from './date-range'
import { Config } from '..'
import { findBestMatchingLiteral, findLabel, i18n } from '../i18n'

export { Facet }
export let labels: Record<string, string> = {}

export function facetFactory(field: Field, schema: Schema, config: Config): Facet | null {
    // exlcude id and internal fields from faceting
    if (field.name === schema.uniqueKey || (field.name.startsWith('_') && field.name !== '_shape')) {
        return null
    }
    switch(field.type) {
        case 'string': return new KeywordFacet(field.name, config)
        case 'location_rpt': return new GeoLocationFacet(field.name)
        case 'plong':
        case 'pint':
        case 'pdouble':
            return new NumberRangeFacet(field.name)
        case 'pdate':
            return new DateRangeFacet(field.name)
    }
    return null
}

export async function initFacets(config: Config, schema: Schema): Promise<Facet[]> {
    labels = await initFacetLabels(config)
    // derive facets from schema
    const facets: Facet[] = []
    for (const field of schema.fields) {
        if (field.type && (field.indexed === undefined || field.indexed === true)) {
            const facet = facetFactory(field, schema, config)
            if (facet) {
                facets.push(facet)
            }
        }
    }

    // let search function set available values and doc counts on the facets
    await search(config.index, { limit: 0, facets: facets })
    return facets
}

async function initFacetLabels(config: Config): Promise<Record<string, string>> {
    const regex = new RegExp(config.solrRegex, 'g')
    const labels: Record<string, string> = { '_shape': i18n['profile'] }
    const nodeShapes = config.shapesGraph.getQuads(null, `${PREFIX_RDF}type`, `${PREFIX_SHACL}NodeShape`, null)
    for (const nodeShape of nodeShapes) {
        // set label for node shape read from dcterms:title or rdfs:label
        let titles = config.shapesGraph.getObjects(nodeShape.subject, `${PREFIX_DCTERMS}title`, null)
        titles = titles.concat(config.shapesGraph.getObjects(nodeShape.subject, `${PREFIX_RDFS}label`, null))
        const title = findBestMatchingLiteral(titles as Literal[])
        // set label for plain subject id (used by values of _shape index column)
        labels[nodeShape.subject.id] = title
        // set label for escaped subject id (used by column names of properties)
        labels[cleanStringForSolr(nodeShape.subject.id, regex, config.solrRegexReplacement)] = title

        const propertyShapes = config.shapesGraph.getQuads(nodeShape.subject, `${PREFIX_SHACL}property`, null, null)
        for (const propertyShape of propertyShapes) {
            const propertyQuads = config.shapesGraph.getQuads(propertyShape.object, null, null, null)
            const label = findLabel(propertyQuads)
            const path = config.shapesGraph.getObjects(propertyShape.object, `${PREFIX_SHACL}path`, null)
            if (label && path?.length === 1) {
                labels[cleanStringForSolr(nodeShape.subject.id, regex, config.solrRegexReplacement) + '.' + cleanStringForSolr(path[0].id, regex, config.solrRegexReplacement)] = label
            }
        }
    }
    return labels
}

function cleanStringForSolr(s: string, regex: RegExp, replacement: string): string {
    return s.toLowerCase().replace(regex, replacement)
}
