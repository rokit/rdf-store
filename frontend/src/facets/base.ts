import { LitElement, css } from 'lit'
import { labels } from '.'
import { AggregationFacet, QueryFacet } from '../solr'
import { property } from 'lit/decorators.js'

export abstract class Facet extends LitElement {
    static styles = [css`
        header { color: #555; display: flex; align-items: center; user-select: none; word-break: break-all; }
        header .label { flex-grow: 1; }
        header:not(.active) .reset-button { visibility: hidden; }
        mdui-icon { color: #888; margin-right: 4px; }
        .w-100 { width: 100% }
    `]

    @property()
    active: boolean = false
    @property()
    valid: boolean = false

    indexField: string
    label?: string

    constructor(indexField: string) {
        super()
        this.indexField = indexField
        this.label = labels[indexField]
        if (this.label) {
            const tokens = indexField.split('.')
            if (tokens.length === 2) {
                const profileLabel = labels[tokens[0]]
                if (profileLabel) {
                    this.label = profileLabel + ' > ' + this.label
                }
            }
        }
    }

    abstract updateValues(aggs: Record<string, AggregationFacet | number | string>): void
    abstract applyAggregationQuery(facets: Record<string, QueryFacet | string>): void
    abstract applyFilterQuery(filter: string[]): void
}
