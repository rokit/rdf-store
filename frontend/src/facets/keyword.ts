import { css, html, nothing } from 'lit'
import 'mdui/components/icon'
import '@ro-kit/ui-widgets/select'

import { labels } from '.'
import { Facet } from './base'
import { AggregationFacet, QueryFacet } from '../solr'
import { customElement, property } from 'lit/decorators.js'
import { Config } from '..'
import { DataFactory } from 'n3'
import { findLabel } from '../i18n'

@customElement('keyword-facet')
export class KeywordFacet extends Facet {
    static styles = [...Facet.styles, css`
        rokit-select { width: 100%; }
        rokit-select::part(facet-count)::after { content: attr(data-count); color: rgb(var(--mdui-color-secondary)); display: block; font-family: monospace; margin-left: 7px; font-size: 12px; }
    `]

    @property()
    values: { value: string | number, docCount: number, label?: string }[] = []
    @property()
    selectedValue = ''
    config: Config

    constructor(indexField: string, config: Config) {
        super(indexField)
        this.config = config
    }

    onChange(ev: Event) {
        // @ts-ignore
        this.selectedValue = ev.target.value
        this.active = (this.selectedValue !== undefined && this.selectedValue.length > 0) || false
        this.dispatchEvent(new Event('change', { bubbles: true }))
    }

    updateValues(aggs: Record<string, AggregationFacet>) {
        const values = []
        const facet = aggs[this.indexField]
        if (facet?.buckets?.length) {
            for (const bucket of facet.buckets) {
                if (bucket.count > 0) {
                    let label = labels[bucket.val]
                    if (!label) {
                        const quads = this.config.shapesGraph.getQuads(DataFactory.namedNode('' + bucket.val), null, null, null)
                        label = findLabel(quads)
                    }
                    values.push({ value: bucket.val, docCount: bucket.count, label: label })
                }
            }
        }
        this.values = values
        this.valid = values.length > 0
    }

    applyAggregationQuery(facets: Record<string, QueryFacet>) {
        facets[this.indexField] = { field: this.indexField, type: 'terms', limit: this.config.solrMaxAggregations }
    }

    applyFilterQuery(filter: string[]) {
        filter.push(`${this.indexField}:${this.selectedValue?.replace(/:/, '\\:')}`)
    }

    render() {
        return this.values.length ? html`
            <rokit-select value="${this.selectedValue}" label="${this.label || this.indexField}" @change="${(ev: Event) => this.onChange(ev)}" clearable>
                <mdui-icon slot="icon" name="list"></mdui-icon>
                ${this.values.map((value) => html`
                    <li data-value="${value.value}">${value.label || value.value}<span class="facet-count" part="facet-count" data-count="${value.docCount}"></span></li>
                `)}
            </rokit-select>
        ` : nothing
    }
}
