import { customElement, property, query } from 'lit/decorators.js'
import { LitElement, css, html, nothing } from 'lit'
import { ShaclForm } from '@ulb-darmstadt/shacl-form/dist/form-default'
import '@ulb-darmstadt/shacl-form/dist/plugins/leaflet'
import 'mdui/components/select'
import 'mdui/components/menu-item'
import 'mdui/components/dialog'
import 'mdui/components/top-app-bar'
import 'mdui/components/top-app-bar-title'
import 'mdui/components/button-icon'
import { BACKEND_URL } from './constants'
import { labels } from './facets'
import { i18n } from './i18n'

@customElement('shacl-editor')
export class Editor extends LitElement {
    static styles = css`
        .error { color: red; white-space: pre; }
        .error:before { content: '\\26a0'; margin-right:8px; }`
    @property()
    rootShapes?: string[]
    @property()
    selectedShape = ""
    @property()
    rdfSubject = ""
    @property()
    rdfNamespace = ""
    @property()
    errorMessage = ""
    @property()
    open = false
    @property()
    valid = false
    @property()
    saving = false
    @query('shacl-form')
    form?: ShaclForm

    /*
    updated() {
        this.form?.setClassInstanceProvider(clazz => {
            console.log('--- resolving', clazz)
            const url = BACKEND_URL + '/sparql/query'
            return new Promise<string>(async (resolve, reject) => {
                const formData = new URLSearchParams()
                // load class instances from all graphs
                formData.append('query', `CONSTRUCT { ?s ?p ?o } WHERE { GRAPH ?g { ?c a <${clazz}> . ?c (<>|!<>)* ?s . ?s ?p ?o }}`)
                try {
                    const resp = await fetch(url, {
                        method: "POST",
                        cache: "no-cache",
                        body: formData
                    })
                    if (resp.status !== 200) {
                        throw new Error('server returned status ' + resp.status)
                    }
                    const result = await resp.text()
                    console.log('--- classes', clazz, result)
                    resolve(result)
                } catch(e) {
                    reject(e)
                }
            })
        })
    }
*/
    async saveRDF(ttl: string) {
        this.saving = true
        const formData = new URLSearchParams()
        formData.append('ttl', ttl)
        try {
            const url = BACKEND_URL + '/resource'
            const resp = await fetch(url, {
                method: this.rdfSubject ? "PUT" : "POST",
                cache: "no-cache",
                body: formData
            })
            if (resp.status < 200 || resp.status > 299) {
                this.errorMessage = "failed saving RDF. Status: " + resp.status
            } else {
                this.close()
                this.dispatchEvent(new CustomEvent('saved'))
            }
        } catch(e) {
            this.errorMessage = '' + e
        } finally {
            this.saving = false
        }
    }

    close() {
        this.open = false
        setTimeout(() => {
            this.valid = false
            this.rdfSubject = ''
            this.selectedShape = ''
            this.errorMessage = ''
            this.dispatchEvent(new CustomEvent('dialogclosed'))
        }, 150)
    }

    render() {
        return html `<mdui-dialog class="editor-dialog" .open="${this.open}">
            <mdui-top-app-bar variant="center-aligned" slot="header">
                <mdui-top-app-bar-title>${this.rdfSubject ? 'Edit' : this.selectedShape ? i18n['new'] + ' ' + labels[this.selectedShape] : i18n['selectprofile']}</mdui-top-app-bar-title>
                <mdui-button-icon icon="close" @click="${() => this.close()}"></mdui-button-icon>
            </mdui-top-app-bar>
            ${this.errorMessage ? html `<div class="error">${this.errorMessage}</div>` :
            this.rdfSubject ? html `
            <shacl-form style="display: block; min-width: 500px;"
                data-values-namespace="${this.rdfNamespace}"
                data-values-url="${BACKEND_URL}/resource/${encodeURIComponent(this.rdfSubject)}"
                data-values-subject="${this.rdfSubject}"
                data-ignore-owl-imports
                data-collapse="open"
                data-generate-node-shape-reference="http://purl.org/dc/terms/conformsTo"
                @change="${(ev: Event) => {
                    this.valid = (ev as CustomEvent).detail?.valid
                }}"></shacl-form>` :
            html`
            <div style="display:${this.selectedShape ? 'none' : 'block'}; min-width: 500px;">
                <mdui-select label="${i18n['profile']}" @change="${(ev: Event) => {
                    const select = (ev.target as HTMLSelectElement)
                    this.selectedShape = select.value
                    select.value = ''
                }}">
                ${this.rootShapes?.map((id) => html`<mdui-menu-item value="${id}">${labels[id] || id}</mdui-menu-item>`)}
                </mdui-select>
            </div>
            <div style="display:${this.selectedShape ? 'block' : 'none'}; min-width: 500px;">
                <shacl-form
                    data-values-namespace="${this.rdfNamespace}"
                    data-ignore-owl-imports
                    data-collapse="open"
                    data-shape-subject="${this.selectedShape}"
                    data-generate-node-shape-reference="http://purl.org/dc/terms/conformsTo"
                    @change="${(ev: Event) => {
                        this.valid = (ev as CustomEvent).detail?.valid
                    }}"></shacl-form>
            </div>
            `}
            <mdui-button id="save-button" slot="action" style="display:${!this.errorMessage && (this.selectedShape || this.rdfSubject) ? 'block' : 'none'};" disabled="${!this.valid || nothing}" loading="${this.saving || nothing}" @click="${() => {
                this.saveRDF(this.form!.serialize())
            }}">Save</mdui-button>
        </mdui-dialog>`
    }
}
