import { customElement, property, query } from 'lit/decorators.js'
import { LitElement, css, html, nothing } from 'lit'
import { range } from 'lit/directives/range.js'
import { map } from 'lit/directives/map.js'
import { DataFactory, Parser, Store } from 'n3'
import { registerPlugin, setSharedShapesGraph } from '@ulb-darmstadt/shacl-form/dist/form-default'
import { LeafletPlugin } from '@ulb-darmstadt/shacl-form/dist/plugins/leaflet'
import '@fontsource/roboto'
import '@fontsource/material-icons'
import 'mdui/components/button-icon'
import 'mdui/components/button'
import 'mdui/components/icon'
import 'mdui/components/linear-progress'
import 'mdui/components/snackbar'
import { Snackbar } from 'mdui/components/snackbar'

import { initFacets, Facet } from './facets'
import { search, Document, fetchSchema, Schema } from './solr'
import { BACKEND_URL } from './constants'
import './editor'
import { i18n } from './i18n'
import { RokitInput } from '@ro-kit/ui-widgets'

export type Config = {
    shapes: string
    rootShapes: string[]
    index: string
    geoDataType: string
    solrRegex: string
    solrRegexReplacement: string
    solrMaxAggregations: number
    shapesGraph: Store
    crud: boolean
    header: string
    rdfNamespace: string
}

@customElement('shacl-search')
export class App extends LitElement {
    static styles = css`
        :host { font-family: "Roboto"; display: flex; width: 100%; }
        #search-filter { min-width: var(--filter-column-width, 300px); max-width: var(--filter-column-width, 300px); display: flex; flex-direction: column; margin-right: 20px; color: #555; }
        #search-filter header .label { flex-grow: 1; }
        #search-filter header:not(.active) .reset-button { visibility: hidden; }
        #search-result { overflow: auto; flex-grow: 1; position: relative; }
        #search-result mdui-linear-progress { position: absolute; width: 100%; }
        #search-result .hits { display: flex; gap: 30px; flex-wrap: wrap; }
        #search-result mdui-card { min-width: 400px; padding: 10px;  }
        #search-result .edit-button { position: absolute; right: 0; top: 0; }
        .secondary { color: rgb(var(--mdui-color-secondary)); }
        .stats { margin-bottom: 10px; display: flex; align-items: center; gap: 5px; }
        .stats span { font-family: monospace; font-size: 14px; }
        .pager { display: flex; flex-wrap: wrap; align-items: center; gap: 4px; justify-content: end; margin: 20px 0; }
        .error { color: red; white-space: pre; }
        .error:before { content: '\\26a0'; margin-right:8px; }
        .pr-1 { padding-right: 4px; }
        .flex-grow-1 { flex-grow: 1; }
    `

    @property()
    offset = 0
    @property()
    limit = 10
    @property()
    searchTerm = ''
    @property()
    searchHits: Document[] = []
    @property()
    loading = true
    @property()
    facets: Facet[] = []
    @property()
    totalHits = 0
    @property()
    errorMessage = ''
    @property()
    editorOpen = false
    @property()
    editorRdfSubject = ''

    @query('#search-field')
    searchField!: RokitInput
    @query('#search-filter')
    searchFilter!: HTMLDivElement
    @query('#snackbar')
    snackbar?: Snackbar

    debounceTimeout: ReturnType<typeof setTimeout> | undefined
    config: Config | undefined
    schema: Schema | undefined

    connectedCallback() {
        super.connectedCallback();
        (async () => {
            try {
                const resp = await fetch(`${BACKEND_URL}/config`)
                if (resp.status !== 200) {
                    throw 'Failed loading config'
                }
                this.config = await resp.json()
                this.dispatchEvent(new CustomEvent('headerinfo', { bubbles: true, cancelable: false, composed: true, detail: { header: this.config?.header } }))
                const quads = this.config?.shapes?.length ? new Parser().parse(this.config.shapes) : []
                this.config!.shapesGraph = new Store()
                const graphId = DataFactory.namedNode('shapes')
                for (const quad of quads) {
                    // filter out list definition quads, since N3 cannot parse lists otherwise
                    if (!(quad.predicate.id === 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type' && quad.object.id === 'http://www.w3.org/1999/02/22-rdf-syntax-ns#List')) {
                        this.config!.shapesGraph.addQuad(quad.subject, quad.predicate, quad.object, graphId)
                    }
                }
                setSharedShapesGraph(this.config!.shapesGraph)

                this.schema = await fetchSchema(this.config!.index)
                this.facets = await initFacets(this.config!, this.schema)
                registerPlugin(new LeafletPlugin({ datatype: this.config!.geoDataType }))
                this.searchFilter.addEventListener('change', () => this.filterChanged())
                this.filterChanged()
            } catch(e) {
                console.log(e)
                this.errorMessage = '' + e
            }
        })()
    }

    filterChanged(fromPager = false) {
        clearTimeout(this.debounceTimeout)
        this.debounceTimeout = setTimeout(async() => {
            try {
                this.loading = true
                this.searchTerm = this.searchField?.value || ''
                if (fromPager) {
                    this.searchFilter.scrollIntoView()
                } else {
                    this.offset = 0
                }
                const searchResult = await search(this.config!.index, { offset: this.offset, limit: this.limit, term: this.searchTerm, facets: this.facets, sort: `${this.schema!.uniqueKey} asc` })
                if (searchResult.error) {
                    throw searchResult.error.msg || searchResult.error.trace
                }
                this.totalHits = searchResult.response.numFound
                this.searchHits = searchResult.response.docs
            } catch(e) {
                console.error(e)
                this.errorMessage = '' + e
            } finally {
                this.loading = false
            }
        }, 20)
    }

    openEditor(subject?: string) {
        this.editorRdfSubject = subject || ''
        this.editorOpen = true
    }

    closeEditor() {
        this.editorRdfSubject = ''
        this.editorOpen = false
    }

    render() {
        return this.errorMessage ?
        html `<div class="error">${this.errorMessage}</div>` :
        html `
        <div id="search-filter">
            <rokit-input id="search-field" label="${i18n['fulltextsearch']}" value="${this.searchTerm}" clearable>
                <mdui-icon slot="icon" name="search"></mdui-icon>
            </rokit-input>
            ${this.facets.map((facet) => facet.valid ? html`${facet}` : '')}
        </div>
        <div id="search-result">
            ${this.loading ? html`<mdui-linear-progress></mdui-linear-progress>` : html`
                <div class="stats">
                    ${this.totalHits < 1 ?
                    html`<span>${i18n['noresults']}</span>` :
                    html`
                        <span>${i18n['results']} <span class="secondary">${this.offset + 1}</span> - <span class="secondary">${this.offset+this.searchHits.length}</span> ${i18n['of']} <span class="secondary">${this.totalHits}</span>:</span>
                    `}
                    ${this.config?.crud ? html`
                    <mdui-button icon="add" variant="outlined" style="margin-left: 20px" @click="${() => { this.openEditor() }}">${i18n['add']}</mdui-button>`: ''}
                </div>
                ${this.totalHits < 1 ? '' : html`
                    <div class="hits">
                    ${this.searchHits.map((hit) => html`
                        <mdui-card variant="outlined">
                            <shacl-form
                                data-view
                                data-loading=""
                                data-ignore-owl-imports
                                data-collapse
                                data-values="${hit._rdf}"
                                data-values-subject="${decodeURIComponent(hit.id)}">
                            </shacl-form>
                            ${this.config?.crud ? html`<mdui-button-icon icon="edit" class="edit-button" @click="${() => { this.openEditor(decodeURIComponent(hit.id)) }}"/>` : nothing }
                        </mdui-card>`
                    )}
                    </div>
                    <div class="pager">
                        ${i18n['pages']}:
                        ${map(range(1, Math.ceil(this.totalHits / this.limit) + 1), (i) => html`
                            <mdui-button variant="${this.offset == this.limit*(i - 1) ? 'filled' : 'text' }" disabled="${this.offset == this.limit*(i - 1) || nothing}" @click="${() => { this.offset=this.limit*(i - 1); this.filterChanged(true)}}">${i}</mdui-button>
                        `)}
                    </div>
                `
                }
            `}
        </div>
        ${this.config?.crud ? html`
            <shacl-editor .rootShapes="${this.config.rootShapes}" .open="${this.editorOpen}" .rdfSubject="${this.editorRdfSubject}" .rdfNamespace="${this.config.rdfNamespace}"
            @saved="${() => {
                if (this.snackbar) {
                    this.snackbar.innerText = "Saved successfully"
                    this.snackbar.open = true
                }
                this.filterChanged()
            }}"
            @dialogclosed="${() => {
                this.closeEditor()
            }}"
            ></shacl-editor>`: ''}
            <mdui-snackbar auto-close-delay="3000" id="snackbar"></mdui-snackbar>
        `
    }
}
