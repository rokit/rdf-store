import { DataFactory, Literal, Quad } from "n3"
import { PREFIX_RDFS, PREFIX_SHACL, PREFIX_SKOS } from "./constants"

// init languages based on browser languages
const languages = [...new Set(navigator.languages.flatMap(lang => {
    if (lang.length > 2) {
        // for each 5 letter lang code (e.g. de-DE) append its corresponding 2 letter code (e.g. de) directly afterwards
        return [lang.toLocaleLowerCase(), lang.substring(0, 2)]
    } 
    return lang
})), ''] // <-- append empty string to accept RDF literals with no language

const appLabels: Record<string, Literal[]> = {
    'fulltextsearch' : [ DataFactory.literal('Full-text search', 'en'), DataFactory.literal('Volltextsuche', 'de') ],
    'profile' : [ DataFactory.literal('Profile', 'en'), DataFactory.literal('Profil', 'de') ],
    'selectprofile' : [ DataFactory.literal('Select profile', 'en'), DataFactory.literal('Profil auswählen', 'de') ],
    'add' : [ DataFactory.literal('Add', 'en'), DataFactory.literal('Hinzufügen', 'de') ],
    'new' : [ DataFactory.literal('New', 'en'), DataFactory.literal('Neu:', 'de') ],
    'results' : [ DataFactory.literal('Results', 'en'), DataFactory.literal('Ergebnisse', 'de') ],
    'noresults' : [ DataFactory.literal('No results', 'en'), DataFactory.literal('Keine Ergebnisse', 'de') ],
    'of' : [ DataFactory.literal('of', 'en'), DataFactory.literal('von', 'de') ],
    'pages' : [ DataFactory.literal('Pages', 'en'), DataFactory.literal('Seiten', 'de') ]
}

export function findLabel(quads: Quad[]): string {
    const label = findLabelByPredicate(quads, `${PREFIX_SHACL}name`) ||
                  findLabelByPredicate(quads, `${PREFIX_SKOS}prefLabel`) ||
                  findLabelByPredicate(quads, `${PREFIX_RDFS}label`) ||
                  findLabelByPredicate(quads, `${PREFIX_SHACL}path`)
    return label ? label.value : ''
}

export function findBestMatchingLiteral(literals: Literal[]): string {
    let candidate: Literal | undefined
    for (const literal of literals) {
        candidate = prioritizeByLanguage(candidate, literal)
    }
    return candidate ? candidate.value : ''
}

function findLabelByPredicate(quads: Quad[], predicate: string): Literal | undefined {
    let candidate: Literal | undefined
    for (const quad of quads) {
        if (quad.predicate.value === predicate) {
            candidate = prioritizeByLanguage(candidate, quad.object as Literal)
        }
    }
    return candidate
}

function prioritizeByLanguage(text1?: Literal, text2?: Literal): Literal | undefined {
    if (text1 === undefined) {
        return text2
    }
    if (text2 === undefined) {
        return text1
    }
    const index1 = languages.indexOf(text1.language)
    if (index1 < 0) {
        return text2
    }
    const index2 = languages.indexOf(text2.language)
    if (index2 < 0) {
        return text1
    }
    return index2 > index1 ? text1 : text2
}

// init static UI labels
export const i18n: Record<string, string> = {}
for (const key in appLabels) {
    i18n[key] = findBestMatchingLiteral(appLabels[key])
}
