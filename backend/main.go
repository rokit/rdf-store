package main

import (
	"embed"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

//go:embed embed/*
var frontend embed.FS

func main() {
	ImportDataGraph()
	if ReindexOnStartup || !CheckIndexExists() {
		go Reindex()
	}

	var router = gin.Default()
	router.SetTrustedProxies(nil)
	router.UseRawPath = true
	corsConfig := cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	})
	router.Use(static.Serve("/", static.EmbedFolder(frontend, "embed")))
	router.Use(corsConfig)
	attachEndPoints(router)
	if err := router.Run(":3000"); err != nil {
		panic(err)
	}
}
