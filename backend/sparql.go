package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
	"text/template"
	"time"

	xmlquery "github.com/antchfx/xquery/xml"
	"github.com/deiu/rdf2go"
)

const timestampFormat = time.RFC3339

var listQuery = `
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT ?s
WHERE {
  GRAPH ?g {
	VALUES ?shape { {{range .Shapes}}{{.Id}} {{end}}}
	?s ?p ?shape .
	FILTER (?p IN (<` + RDF_TYPE.RawValue() + `>, <` + DCTERMS_CONFORMS_TO.RawValue() + `>)) .
	{{if .Since}}
	?s <http://purl.org/dc/terms/modified> ?date . FILTER( ?date >= xsd:dateTime("{{.Since}}") ) .
	{{- end}}
  }
}
`
var singleGraphQuery = `CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <%s> { ?s ?p ?o } }`
var allGraphsQuery = `CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <%s> { ?s (<>|!<>)* ?s . GRAPH ?g { ?s ?p ?o }}}`
var createQuery = `INSERT DATA { GRAPH <%s> { %s }}`
var deleteQuery = `DROP GRAPH <%s>`
var listQueryTemplate = template.Must(template.New("listQuery").Funcs(template.FuncMap{}).Parse(listQuery))
var blankNodeCounter = 0

type MPSSearchResultItem struct {
	Name        string `json:"name"`
	BaseUrl     string `json:"base_url"`
	Definition  string `json:"definition"`
	Description string `json:"description"`
}

type SparqlResult struct {
	XMLName xml.Name      `xml:"http://www.w3.org/2005/sparql-results# sparql"`
	Results []interface{} `xml:"results"`
}

func ImportDataGraph() error {
	if files, err := os.ReadDir("datagraph"); err == nil {
		for _, file := range files {
			if !file.IsDir() && strings.HasSuffix(file.Name(), ".ttl") {
				fmt.Println("importing data graph file", file.Name())
				resp, err := uploadMultipartFile(nil, FusekiDatasetURL+"/data", file.Name(), "datagraph", "file", "Basic: "+FusekiAuth)
				if err != nil {
					return err
				}
				defer resp.Body.Close()
				if resp.StatusCode != 200 {
					return fmt.Errorf("failed uploading data graph file '%s', response status code was %d", file.Name(), resp.StatusCode)
				}
			}
		}
	}
	return nil
}

func FetchProfiles() (graph *rdf2go.Graph, err error) {
	graph = rdf2go.NewGraph("")
	var profiles []MPSSearchResultItem

	// load profiles from NFDI4Ing metadata profile service
	if MPSEnabled {
		var resp *http.Response
		fmt.Println("loading remote profiles from", MPSEndpoint)
		resp, err = http.Get(MPSEndpoint)
		if err != nil {
			return
		}
		defer resp.Body.Close()
		var data []byte
		data, err = io.ReadAll(resp.Body)
		if err != nil {
			return
		}
		if err = json.Unmarshal(data, &profiles); err != nil {
			return
		}
	}
	// load profiles locally
	if LocalProfilesEnabled {
		if files, err := os.ReadDir("profiles"); err == nil {
			for _, file := range files {
				if !file.IsDir() && strings.HasSuffix(file.Name(), ".ttl") {
					fmt.Println("loading local profile", file.Name())
					if fileContent, err := os.ReadFile(path.Join("profiles", file.Name())); err == nil {
						profiles = append(profiles, MPSSearchResultItem{
							Name:       file.Name(),
							Definition: string(fileContent),
						})
					}
				}
			}
		} else {
			fmt.Println("couldn't read local profiles. reason:", err)
		}
	}

	for _, profile := range profiles {
		subGraph := rdf2go.NewGraph("")
		if err = subGraph.Parse(bytes.NewReader([]byte(profile.Definition)), "text/turtle"); err != nil {
			return
		}
		convertBlankNodes(subGraph, graph)
	}

	// try to load owl:imports
	loadedImports := make(map[string]bool)
	for _, importsStatement := range graph.All(nil, OWL_IMPORTS, nil) {
		// ok, that's a bit blue-eyed: we'll assume that if we already have a triple with the given subject in the graph,
		// then we don't have to load it (again). This will be the case for shacl root shapes, that depend (owl:imports) on each other.
		if graph.One(importsStatement.Object, nil, nil) == nil {
			if _, ok := loadedImports[importsStatement.Object.RawValue()]; !ok {
				loadedImports[importsStatement.Object.RawValue()] = true
				if ignoredError := FetchRDF(importsStatement.Object.RawValue(), graph); ignoredError != nil {
					log.Println(ignoredError)
				}
			}
		}
	}
	return
}

func FindRootShapes(shapesGraph *rdf2go.Graph) (rootShapes []NodeShape) {
	nodeShapesMap := make(map[string]NodeShape)
	propertyMap := make(map[string]*Property)
	var rootShapeIds []rdf2go.Term
	for _, id := range RootProfiles {
		rootShapeIds = append(rootShapeIds, rdf2go.NewResource(id))
	}
	if len(rootShapeIds) == 0 {
		// find root shapes. root shapes are node shapes that are not referenced by SHACL properties via sh:node.
		for _, nodeShapeTriple := range shapesGraph.All(nil, RDF_TYPE, SHACL_NODE_SHAPE) {
			reference := shapesGraph.One(nil, SHACL_NODE, nodeShapeTriple.Subject)
			if reference == nil || shapesGraph.One(reference.Subject, RDF_TYPE, SHACL_NODE_SHAPE) != nil {
				rootShapeIds = append(rootShapeIds, nodeShapeTriple.Subject)
			}
		}
	}
	for _, rootShapeId := range rootShapeIds {
		rootShapes = append(rootShapes, new(NodeShape).Parse(rootShapeId, shapesGraph, nodeShapesMap, propertyMap))
	}
	return
}

func FetchDatasets(since *time.Time) (ids []string, err error) {
	var rootShapes []NodeShape
	for _, shape := range rootShapesMap {
		rootShapes = append(rootShapes, shape)
	}
	listTmplInput := map[string]interface{}{
		"Shapes": rootShapes,
	}
	if since != nil {
		listTmplInput["Since"] = since.Format(timestampFormat)
	}

	buf := &bytes.Buffer{}
	if err = listQueryTemplate.Execute(buf, listTmplInput); err != nil {
		return
	}

	body := url.Values{}
	body.Set("query", buf.String())

	resp, err := http.Post(FusekiDatasetURL, "application/x-www-form-urlencoded", strings.NewReader(body.Encode()))
	if err != nil {
		return
	}
	defer resp.Body.Close()

	doc, err := xmlquery.Parse(resp.Body)
	if err != nil {
		return
	}
	xmlquery.FindEach(doc, "//uri", func(ctr int, node *xmlquery.Node) {
		ids = append(ids, node.InnerText())
	})
	return
}

func StoreDataGraph(graph *rdf2go.Graph) (id rdf2go.Term, err error) {
	if !config.CRUD {
		err = errors.New("not allowed")
		return
	}

	id, _, err = findDatasetShape(graph, nil)
	if err != nil {
		return
	}

	buf := new(bytes.Buffer)
	if err = graph.Serialize(buf, "text/turtle"); err != nil {
		return
	}

	form := url.Values{}
	form.Set("update", fmt.Sprintf(createQuery, id.RawValue(), buf.String()))
	req, err := http.NewRequest("POST", FusekiDatasetUpdateURL, strings.NewReader(form.Encode()))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", "Basic: "+FusekiAuth)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		message := ""
		if body, err := io.ReadAll(resp.Body); err == nil {
			message = string(body)
		}
		return nil, fmt.Errorf("failed storing in triplestore - status: %d, message: '%s'", resp.StatusCode, message)
	}

	graph, err = FetchDataGraph(id.RawValue(), true)
	if err != nil {
		return
	}

	if err = IndexRDF(graph, id); err != nil {
		return
	}

	return
}

func UpdateDataGraph(graph *rdf2go.Graph) (id rdf2go.Term, err error) {
	if !config.CRUD {
		err = errors.New("not allowed")
		return
	}

	id, _, err = findDatasetShape(graph, nil)
	if err != nil {
		return
	}

	if err = DeleteDataGraph(id); err != nil {
		return
	}
	return StoreDataGraph(graph)
}

func DeleteDataGraph(id rdf2go.Term) (err error) {
	if !config.CRUD {
		err = errors.New("not allowed")
		return
	}
	form := url.Values{}
	form.Set("update", fmt.Sprintf(deleteQuery, id.RawValue()))
	req, err := http.NewRequest("POST", FusekiDatasetUpdateURL, strings.NewReader(form.Encode()))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", "Basic: "+FusekiAuth)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		message := ""
		if body, err := io.ReadAll(resp.Body); err == nil {
			message = string(body)
		}
		return fmt.Errorf("failed deleting in triplestore - status: %d, message: '%s'", resp.StatusCode, message)
	}
	return
}

func FetchDataGraph(id string, union bool) (graph *rdf2go.Graph, err error) {
	graph = rdf2go.NewGraph("")
	var query string
	if union {
		query = fmt.Sprintf(allGraphsQuery, id)
	} else {
		query = fmt.Sprintf(singleGraphQuery, id)
	}
	fmt.Println("--- query", query)
	body := url.Values{}
	body.Set("query", query)
	resp, err := http.Post(FusekiDatasetURL, "application/x-www-form-urlencoded", strings.NewReader(body.Encode()))
	if err != nil {
		return
	}
	defer resp.Body.Close()

	err = graph.Parse(resp.Body, "text/turtle")
	return
}

func FetchRDF(url string, graph *rdf2go.Graph) (err error) {
	fmt.Println("loading external URL", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Add("Accept", "application/n-triples")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	rdf, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	// replace blank nodes with unique ids to prevent collision with existing graphs
	subGraph := rdf2go.NewGraph("")
	if err = subGraph.Parse(bytes.NewReader(rdf), "text/turtle"); err != nil {
		return
	}
	convertBlankNodes(subGraph, graph)

	return
}

func PreresolveClassInstances(graph *rdf2go.Graph) error {
	var classes []string
	for _, triple := range graph.All(nil, SHACL_CLASS, nil) {
		classes = append(classes, triple.Object.String())
	}
	if len(classes) > 0 {
		query := &bytes.Buffer{}
		if err := template.Must(template.New("").Parse(`CONSTRUCT { ?s ?p ?o } WHERE { VALUES ?clazz { {{range .Classes}}{{.}} {{end}}} ?s a ?clazz . ?s ?p ?o }`)).Execute(query, map[string]interface{}{
			"Classes": classes,
		}); err != nil {
			return err
		}
		body := url.Values{}
		body.Set("query", query.String())
		resp, err := http.Post(FusekiDatasetURL, "application/x-www-form-urlencoded", strings.NewReader(body.Encode()))
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		if err = graph.Parse(resp.Body, "text/turtle"); err != nil {
			return err
		}
	}
	return nil
}

/* replaces blank node ids with internal unqique ids to prevent collision */
func convertBlankNodes(sourceGraph *rdf2go.Graph, targetGraph *rdf2go.Graph) {
	blankNodes := make(map[string]*rdf2go.BlankNode)
	for t := range sourceGraph.IterTriples() {
		switch v := t.Subject.(type) {
		case *rdf2go.BlankNode:
			replacement, ok := blankNodes[v.ID]
			if !ok {
				blankNodeCounter = blankNodeCounter + 1
				replacement = &rdf2go.BlankNode{ID: fmt.Sprintf("b%d", blankNodeCounter)}
				blankNodes[v.ID] = replacement
			}
			t.Subject = replacement
		}
		switch v := t.Object.(type) {
		case *rdf2go.BlankNode:
			replacement, ok := blankNodes[v.ID]
			if !ok {
				blankNodeCounter = blankNodeCounter + 1
				replacement = &rdf2go.BlankNode{ID: fmt.Sprintf("b%d", blankNodeCounter)}
				blankNodes[v.ID] = replacement
			}
			t.Object = replacement
		}
		targetGraph.AddTriple(t.Subject, t.Predicate, t.Object)
	}
}
