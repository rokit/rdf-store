package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var SolrEndpoint = EnvVar("SOLR_ENDPOINT", "http://localhost:8983")
var SolrIndex = EnvVar("SOLR_INDEX", "rdf")
var SolrNumShards = EnvVarAsInt("SOLR_NUM_SHARDS", 1)

var FusekiEndpoint = EnvVar("FUSEKI_ENDPOINT", "http://localhost:3030")
var FusekiDataset = EnvVar("FUSEKI_DATASET", "rdf")
var FusekiDatasetURL = fmt.Sprintf("%s/%s", FusekiEndpoint, FusekiDataset)
var FusekiDatasetUpdateURL = FusekiDatasetURL + "/update"
var FusekiUser = EnvVar("FUSEKI_USER", "admin")
var FusekiPass = EnvVar("FUSEKI_PASSWORD", "secret")
var FusekiAuth = base64.StdEncoding.EncodeToString([]byte(FusekiUser + ":" + FusekiPass))
var ExposeFusekiFrontend = EnvVarAsBool("EXPOSE_FUSEKI_FRONTEND", false)

var ReindexOnStartup = EnvVarAsBool("REINDEX_ON_STARTUP", true)
var LocalProfilesEnabled = EnvVarAsBool("LOCAL_PROFILES_ENABLED", true)
var RootProfiles = EnvVarAsStringSlice("ROOT_PROFILES")
var MPSEnabled = EnvVarAsBool("MPS_ENABLED", false)
var MPSQuery = EnvVar("MPS_QUERY", "kit")
var MPSLanguage = EnvVar("MPS_LANGAGE", "EN")
var MPSEndpoint = EnvVar("MPS_ENDPOINT", fmt.Sprintf("https://pg4aims.ulb.tu-darmstadt.de/AIMS/application-profiles/?query=%s&language=%s&includeDefinition=true&state=public", MPSQuery, MPSLanguage))

func EnvVar(key string, defaultValue string) string {
	if val, present := os.LookupEnv(key); present {
		return val
	}
	return defaultValue
}

func EnvVarAsInt(key string, defaultValue int) int {
	if val, present := os.LookupEnv(key); present {
		res, err := strconv.Atoi(val)
		if err != nil {
			log.Printf("warning: env var '%s' with value '%s' is not an integer. using default: %d\n", key, val, defaultValue)
			return defaultValue
		} else {
			return res
		}
	}
	return defaultValue
}

func EnvVarAsBool(key string, defaultValue bool) bool {
	if val, present := os.LookupEnv(key); present {
		res, err := strconv.ParseBool(val)
		if err != nil {
			log.Printf("warning: env var '%s' with value '%s' is not a boolean. using default: %v\n", key, val, defaultValue)
			return defaultValue
		} else {
			return res
		}
	}
	return defaultValue
}

func EnvVarAsStringSlice(key string) []string {
	var result []string
	if val, present := os.LookupEnv(key); present {
		for _, v := range strings.Split(val, ",") {
			result = append(result, strings.TrimSpace(v))
		}
	}
	return result
}
