package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"maps"
	"net/http"
	"net/url"

	"github.com/deiu/rdf2go"
	"github.com/stevenferrer/solr-go"
)

var solrClient = solr.NewJSONClient(SolrEndpoint)
var shapesGraph *rdf2go.Graph
var rootShapesMap = make(map[string]NodeShape)

type Config struct {
	Shapes               string   `json:"shapes"`
	RootShapes           []string `json:"rootShapes"`
	Index                string   `json:"index"`
	GeoDataType          string   `json:"geoDataType"`
	SolrRegex            string   `json:"solrRegex"`
	SolrRegexReplacement string   `json:"solrRegexReplacement"`
	SolrMaxAggregations  int      `json:"solrMaxAggregations"`
	CRUD                 bool     `json:"crud"`
	HeaderInfo           string   `json:"header"`
	RdfNamespace         string   `json:"rdfNamespace"`
}

var config = Config{
	Index:                SolrIndex,
	GeoDataType:          "http://www.opengis.net/ont/geosparql#wktLiteral",
	SolrRegex:            `[\/*?"<>|#:.\- ]`,
	SolrRegexReplacement: "_",
	SolrMaxAggregations:  1000,
	CRUD:                 EnvVarAsBool("CRUD", true),
	HeaderInfo:           EnvVar("HEADER_INFO", ""),
	RdfNamespace:         EnvVar("RDF_NAMESPACE", "http://example.org/"),
}

func init() {
	var err error
	if shapesGraph, err = FetchProfiles(); err != nil {
		panic(err)
	}
	if err = PreresolveClassInstances(shapesGraph); err != nil {
		panic(err)
	}
	buf := new(bytes.Buffer)
	if err := shapesGraph.Serialize(buf, "text/turtle"); err != nil {
		panic(err)
	}

	config.Shapes = buf.String()
	rootShapes := FindRootShapes(shapesGraph)
	for _, rootShape := range rootShapes {
		rootShapesMap[rootShape.Id.RawValue()] = rootShape
		config.RootShapes = append(config.RootShapes, rootShape.Id.RawValue())
	}
}

func CheckIndexExists() bool {
	query := solr.Query{}
	_, err := solrClient.Query(context.Background(), SolrIndex, &query)
	return err == nil
}

func Reindex() {
	if err := RecreateCollection(); err != nil {
		panic(err)
	}

	datasets, err := FetchDatasets(nil)
	if err != nil {
		panic(err)
	}
	for _, id := range datasets {
		graph, err := FetchDataGraph(id, true)
		if err != nil {
			panic(err)
		}
		if graph.Len() == 0 {
			break
		}
		if err = IndexRDF(graph, rdf2go.NewResource(id)); err != nil {
			log.Println(err)
		}
	}
}

func RecreateCollection() error {
	fields, copyFields := CreateSchema(maps.Values(rootShapesMap))
	log.Printf(`Recreating collection "%s" on "%s"`, SolrIndex, SolrEndpoint)
	if err := solrClient.DeleteCollection(context.Background(), solr.NewCollectionParams().Name(SolrIndex)); err != nil {
		fmt.Println("collection couldn't be deleted. reason:", err)
	}
	if err := solrClient.CreateCollection(context.Background(), solr.NewCollectionParams().Name(SolrIndex).NumShards(SolrNumShards)); err != nil {
		return err
	}
	if err := solrClient.AddFields(context.Background(), SolrIndex, fields...); err != nil {
		return err
	}
	if err := solrClient.AddCopyFields(context.Background(), SolrIndex, copyFields...); err != nil {
		return err
	}
	if err := patchLocationField(); err != nil {
		return err
	}
	return nil
}

func IndexRDF(graph *rdf2go.Graph, id rdf2go.Term) error {
	id, shape, err := findDatasetShape(graph, id)
	if err != nil {
		return err
	}
	log.Printf(`Indexing dataset "%s" with node shape "%s"`, id, shape.Name)

	var buf bytes.Buffer
	if err := graph.Serialize(&buf, "text/turtle"); err != nil {
		return err
	}
	doc := map[string][]interface{}{
		"_rdf":   {buf.String()},
		"_shape": {shape.Id.RawValue()},
		"id":     {url.QueryEscape(id.RawValue())},
	}
	appendPropertiesToDoc(id, doc, *shape, graph)

	cmd := map[string]interface{}{"add": map[string]interface{}{"doc": doc}}
	marshalled, err := json.Marshal(cmd)
	if err != nil {
		return err
	}
	resp, err := solrClient.Update(context.Background(), SolrIndex, solr.JSON, bytes.NewReader(marshalled))
	if err != nil {
		return err
	}
	if resp.Error != nil {
		return errors.New(resp.Error.Msg)
	}
	if err := solrClient.Commit(context.Background(), SolrIndex); err != nil {
		return err
	}

	return nil
}

func appendPropertiesToDoc(subject rdf2go.Term, doc map[string][]interface{}, shape NodeShape, data *rdf2go.Graph) {
	for _, parent := range shape.Parents {
		appendPropertiesToDoc(subject, doc, parent, data)
	}
	for _, property := range shape.Properties {
		if property.Path != nil {
			values := data.All(subject, property.Path, nil)
			if len(values) > 0 {
				propertyName := fmt.Sprintf("%s.%s", cleanStringForSolr(shape.Id.RawValue()), cleanStringForSolr(property.Path.RawValue()))

				array := make([]interface{}, 0)
				for _, value := range values {
					if property.NodeShape != nil {
						appendPropertiesToDoc(value.Object, doc, *property.NodeShape, data)
					} else {
						array = append(array, value.Object.RawValue())
					}
				}
				doc[propertyName] = append(doc[propertyName], array)
			}
		}
	}
}

// This enables WKT polygon indexing. Note that we have installed "jts-core" in our docker image.
// See https://solr.apache.org/guide/solr/latest/query-guide/spatial-search.html#jts-and-polygons-flat
func patchLocationField() error {
	body := map[string]interface{}{
		"replace-field-type": map[string]interface{}{
			"name":                  "location_rpt",
			"class":                 "solr.SpatialRecursivePrefixTreeFieldType",
			"spatialContextFactory": "JTS",
			"autoIndex":             "true",
			"validationRule":        "repairBuffer0",
			"distErrPct":            "0.025",
			"maxDistErr":            "0.001",
			"distanceUnits":         "kilometers",
		},
	}
	data, err := json.Marshal(body)
	if err != nil {
		return err
	}
	// since solr-go doesn't support this we'll simply post directly to solr
	_, err = http.Post(fmt.Sprintf("%s/solr/%s/schema", SolrEndpoint, SolrIndex), "application/json", bytes.NewReader(data))
	return err
}

func findDatasetShape(graph *rdf2go.Graph, id rdf2go.Term) (datasetID rdf2go.Term, shape *NodeShape, err error) {
	refs := graph.All(id, DCTERMS_CONFORMS_TO, nil)
	refs = append(refs, graph.All(id, RDF_TYPE, nil)...)
	for _, triple := range refs {
		if shapeRef, ok := rootShapesMap[triple.Object.RawValue()]; ok {
			if id != nil && !triple.Subject.Equal(id) {
				return nil, nil, errors.New("ARGH")
			} else if datasetID != nil {
				return nil, nil, errors.New("graph has multiple relations " + DCTERMS_CONFORMS_TO.String() + " or " + RDF_TYPE.String() + " to a known root shape")
			}
			datasetID = triple.Subject
			shape = &shapeRef
		}
	}
	if datasetID == nil {
		return nil, nil, errors.New("graph has no relation " + DCTERMS_CONFORMS_TO.String() + " or " + RDF_TYPE.String() + " to a known root shape")
	}
	return
}
