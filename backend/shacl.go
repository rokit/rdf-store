package main

import (
	"fmt"
	"strconv"

	"github.com/deiu/rdf2go"
)

type NodeShape struct {
	Id         rdf2go.Term
	Name       string
	Parents    []NodeShape
	Properties []Property
}

func (node *NodeShape) Parse(id rdf2go.Term, graph *rdf2go.Graph, nodeShapesMap map[string]NodeShape, propertyMap map[string]*Property) NodeShape {
	if v, ok := nodeShapesMap[id.String()]; ok {
		return v
	}
	node.Id = id

	if label := findLabel(id, graph); label != nil {
		node.Name = *label
	} else {
		node.Name = id.RawValue()
	}
	for _, parentShape := range graph.All(id, SHACL_AND, nil) {
		shape := new(NodeShape).Parse(parentShape.Object, graph, nodeShapesMap, propertyMap)
		nodeShapesMap[parentShape.Object.String()] = shape
		node.Parents = append(node.Parents, shape)
	}
	if parentShape := graph.One(id, SHACL_NODE, nil); parentShape != nil {
		shape := new(NodeShape).Parse(parentShape.Object, graph, nodeShapesMap, propertyMap)
		nodeShapesMap[parentShape.Object.String()] = shape
		node.Parents = append(node.Parents, shape)
	}
	for _, property := range graph.All(id, SHACL_PROPERTY, nil) {
		node.Properties = append(node.Properties, *new(Property).Parse(property.Object, node, graph, nodeShapesMap, propertyMap))
	}
	return *node
}

type Property struct {
	Id         rdf2go.Term
	Label      string
	ShapeLabel string
	ShapeId    string
	FieldName  string
	Path       *rdf2go.Resource
	Datatype   *rdf2go.Resource
	In         bool
	NodeShape  *NodeShape
	Class      *rdf2go.Resource
	NodeKind   *rdf2go.Resource
	Facet      *bool
}

func (prop *Property) Parse(id rdf2go.Term, parent *NodeShape, graph *rdf2go.Graph, nodeShapesMap map[string]NodeShape, propertyMap map[string]*Property) *Property {
	if v, ok := propertyMap[id.String()]; ok {
		return v
	}
	prop.Id = id
	prop.ShapeId = parent.Id.RawValue()
	if label := findLabel(id, graph); label != nil {
		prop.Label = *label
	} else {
		prop.Label = id.RawValue()
	}
	if shapeLabel := findLabel(parent.Id, graph); shapeLabel != nil {
		prop.ShapeLabel = *shapeLabel
	}
	if datatype := graph.One(id, SHACL_DATATYPE, nil); datatype != nil {
		if spec, ok := datatype.Object.(*rdf2go.Resource); ok {
			prop.Datatype = spec
		} else {
			panic(fmt.Errorf("property's sh:datatype is not a named node: %v", datatype.Object))
		}
	}
	if path := graph.One(id, SHACL_PATH, nil); path != nil {
		if spec, ok := path.Object.(*rdf2go.Resource); ok {
			prop.Path = spec
			nodeName := cleanStringForSolr(parent.Id.RawValue())
			prop.FieldName = fmt.Sprintf("%s.%s", nodeName, cleanStringForSolr(prop.Path.RawValue()))
		}
	}
	if node := graph.One(id, SHACL_NODE, nil); node != nil {
		shape := new(NodeShape).Parse(node.Object, graph, nodeShapesMap, propertyMap)
		prop.NodeShape = &shape
	}
	if class := graph.One(id, SHACL_CLASS, nil); class != nil {
		if spec, ok := class.Object.(*rdf2go.Resource); ok {
			prop.Class = spec
		} else {
			panic(fmt.Errorf("property's sh:class is not a named node: %v", class.Object))
		}
	}
	if in := graph.One(id, SHACL_IN, nil); in != nil {
		prop.In = true
	}
	if nodeKind := graph.One(id, SHACL_NODE_KIND, nil); nodeKind != nil {
		if spec, ok := nodeKind.Object.(*rdf2go.Resource); ok {
			prop.NodeKind = spec
		}
	}
	if facet := graph.One(id, DASH_FACET, nil); facet != nil {
		if spec, ok := facet.Object.(*rdf2go.Literal); ok {
			boolValue, err := strconv.ParseBool(spec.RawValue())
			if err != nil {
				panic(fmt.Errorf("property's dash:facet is not a boolean: %v", spec.RawValue()))
			}

			prop.Facet = &boolValue
		}
	}
	propertyMap[id.String()] = prop
	return prop
}

func findLabel(id rdf2go.Term, graph *rdf2go.Graph) *string {
	var result string
	if label := graph.One(id, SHACL_NAME, nil); label != nil {
		result = label.Object.RawValue()
	} else if label := graph.One(id, SKOS_PREF_LABEL, nil); label != nil {
		result = label.Object.RawValue()
	} else if label := graph.One(id, RDFS_LABEL, nil); label != nil {
		result = label.Object.RawValue()
	} else if label := graph.One(id, DCTERMS_TITLE, nil); label != nil {
		result = label.Object.RawValue()
	}
	if len(result) > 0 {
		return &result
	}
	return nil
}
