package main

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"github.com/deiu/rdf2go"
	"github.com/gin-gonic/gin"
)

type JSONError struct {
	Error string `json:"error"`
}

var basePath = "/api/v1"
var solrProxy *httputil.ReverseProxy
var solrProxyTarget *url.URL
var fusekiProxy *httputil.ReverseProxy
var fusekiProxyTarget *url.URL

func attachEndPoints(router *gin.Engine) {
	router.GET(basePath+"/config", handleConfig)
	router.GET(basePath+"/resource/:id", handleGetResource)
	router.POST(basePath+"/resource", handleAddResource)
	router.PUT(basePath+"/resource", handleUpdateResource)
	router.GET(basePath+"/solr/:collection/schema", handleSolr)
	router.GET(basePath+"/solr/:collection/query", handleSolr)
	router.POST(basePath+"/solr/:collection/query", handleSolr)
	router.POST(basePath+"/sparql/query", handleFusekiSparql)
	if ExposeFusekiFrontend {
		router.Any("/fuseki/*proxyPath", handleFuseki)
	}

	var err error
	// init solr proxy
	solrProxyTarget, err = url.Parse(SolrEndpoint)
	if err != nil {
		panic(err)
	}
	solrProxy = httputil.NewSingleHostReverseProxy(solrProxyTarget)

	// init fuseki proxy
	fusekiProxyTarget, err = url.Parse(FusekiEndpoint)
	if err != nil {
		panic(err)
	}
	fusekiProxy = httputil.NewSingleHostReverseProxy(fusekiProxyTarget)
	fusekiProxy.ModifyResponse = func(resp *http.Response) error {
		// delete CORS headers sent by the sparql endpoint. we're setting these ourselves in the http handler chain.
		// not deleting the headers will produce e.g. "CORS Multiple Origin Not Allowed" errors in the browser.
		resp.Header.Del("Access-Control-Allow-Origin")
		resp.Header.Del("Access-Control-Allow-Credentials")
		return nil
	}
}

func handleConfig(c *gin.Context) {
	c.JSON(http.StatusOK, config)
}

func handleGetResource(c *gin.Context) {
	id := c.Param("id")
	did, err := url.QueryUnescape(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	graph, err := FetchDataGraph(did, false)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var buf bytes.Buffer
	if err := graph.Serialize(&buf, "text/turtle"); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.Data(http.StatusOK, "text/turtle", buf.Bytes())
}

func handleAddResource(c *gin.Context) {
	graph, err := readGraphFromRequest(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err = StoreDataGraph(graph)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.String(http.StatusNoContent, "")
}

func handleUpdateResource(c *gin.Context) {
	graph, err := readGraphFromRequest(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err = UpdateDataGraph(graph)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.String(http.StatusNoContent, "")
}

func readGraphFromRequest(c *gin.Context) (graph *rdf2go.Graph, err error) {
	if ttl := c.PostForm("ttl"); ttl != "" {
		graph = rdf2go.NewGraph("")
		err = graph.Parse(bytes.NewReader([]byte(ttl)), "text/turtle")
	} else {
		err = errors.New("no ttl form param")
	}
	return
}

func handleSolr(c *gin.Context) {
	c.Request.URL.Path = strings.TrimPrefix(c.Request.URL.Path, basePath)
	c.Request.URL.Scheme = solrProxyTarget.Scheme
	c.Request.URL.Host = solrProxyTarget.Host
	c.Request.Host = solrProxyTarget.Host
	c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
	solrProxy.ServeHTTP(c.Writer, c.Request)
}

func handleFusekiSparql(c *gin.Context) {
	c.Request.URL.Path = fmt.Sprintf("/%s/query", FusekiDataset)
	c.Request.URL.Scheme = fusekiProxyTarget.Scheme
	c.Request.URL.Host = fusekiProxyTarget.Host
	c.Request.Host = fusekiProxyTarget.Host
	c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
	c.Request.Header.Set("Authorization", "Basic: "+FusekiAuth)
	fusekiProxy.ServeHTTP(c.Writer, c.Request)
}

func handleFuseki(c *gin.Context) {
	c.Request.URL.Path = fusekiProxyTarget.Path + c.Param("proxyPath")
	c.Request.URL.Scheme = fusekiProxyTarget.Scheme
	c.Request.URL.Host = fusekiProxyTarget.Host
	c.Request.Host = fusekiProxyTarget.Host
	c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
	fusekiProxy.ServeHTTP(c.Writer, c.Request)
}
