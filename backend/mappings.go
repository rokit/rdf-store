package main

import (
	"fmt"
	"iter"
	"regexp"
	"strings"

	"github.com/stevenferrer/solr-go"
)

var datatypeMappings = map[string]string{
	fmt.Sprintf(prefixXSD, "string"):        "text_general",
	fmt.Sprintf(prefixXSD, "integer"):       "pint",
	fmt.Sprintf(prefixXSD, "int"):           "pint",
	fmt.Sprintf(prefixXSD, "short"):         "pint",
	fmt.Sprintf(prefixXSD, "long"):          "plong",
	fmt.Sprintf(prefixXSD, "byte"):          "pint",
	fmt.Sprintf(prefixXSD, "unsignedLong"):  "plong",
	fmt.Sprintf(prefixXSD, "unsignedInt"):   "pint",
	fmt.Sprintf(prefixXSD, "unsignedShort"): "pint",
	fmt.Sprintf(prefixXSD, "unsignedByte"):  "pint",
	fmt.Sprintf(prefixXSD, "float"):         "pfloat",
	fmt.Sprintf(prefixXSD, "double"):        "pdouble",
	fmt.Sprintf(prefixXSD, "decimal"):       "pdouble",
	fmt.Sprintf(prefixXSD, "date"):          "pdate",
	fmt.Sprintf(prefixXSD, "dateTime"):      "pdate",
	fmt.Sprintf(prefixXSD, "boolean"):       "pboolean",
	config.GeoDataType:                      "location_rpt",
}

var solrStringReplacer = regexp.MustCompile(config.SolrRegex)

func CreateSchema(rootShapes iter.Seq[NodeShape]) ([]solr.Field, []solr.CopyField) {
	fields := make([]solr.Field, 0)
	fields = append(fields, solr.Field{Name: "_rdf", Type: "string", Indexed: false, Stored: true})
	fields = append(fields, solr.Field{Name: "_shape", Type: "string", Indexed: true, Stored: false})
	for nodeShape := range rootShapes {
		createPropertyMappings(nodeShape.Properties, &fields)
	}

	copyFields := make([]solr.CopyField, 0)
	copyFields = append(copyFields, solr.CopyField{Source: "*", Dest: "_text_"})
	return fields, copyFields
}

func createPropertyMappings(properties []Property, mappings *[]solr.Field) {
	for _, property := range properties {
		if property.Path != nil {
			if property.NodeShape != nil {
				createPropertyMappings(property.NodeShape.Properties, mappings)
			} else if property.Facet == nil || *property.Facet {
				if property.Class != nil || property.In || (property.NodeKind != nil && SHACL_IRI.Equal(property.NodeKind)) || (property.Facet != nil && *property.Facet) {
					// these are supposed to be facets
					*mappings = append(*mappings, solr.Field{Name: property.FieldName, Type: "string", MultiValued: true})
				} else {
					var datatype string
					if property.Datatype != nil {
						if value, ok := datatypeMappings[property.Datatype.RawValue()]; ok {
							datatype = value
						}
					}
					if datatype == "" {
						// fall back to text if no specific datatype is known
						datatype = "text_general"
					}
					*mappings = append(*mappings, solr.Field{Name: property.FieldName, Type: datatype, MultiValued: true})
				}
			}
		}
	}
}

func cleanStringForSolr(s string) string {
	return solrStringReplacer.ReplaceAllString(strings.ToLower(s), config.SolrRegexReplacement)
}
