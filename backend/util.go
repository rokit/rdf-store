package main

import (
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path"
)

func uploadMultipartFile(client *http.Client, uri string, filename string, dir string, key string, authHeader string) (*http.Response, error) {
	body, writer := io.Pipe()

	req, err := http.NewRequest(http.MethodPost, uri, body)
	if err != nil {
		return nil, err
	}

	mwriter := multipart.NewWriter(writer)
	req.Header.Add("Content-Type", mwriter.FormDataContentType())
	if len(authHeader) > 0 {
		req.Header.Set("Authorization", authHeader)
	}

	errchan := make(chan error)

	go func() {
		defer close(errchan)
		defer writer.Close()
		defer mwriter.Close()

		w, err := mwriter.CreateFormFile(key, filename)
		if err != nil {
			errchan <- err
			return
		}
		reader, err := os.Open(path.Join(dir, filename))
		if err != nil {
			errchan <- err
			return
		}
		defer reader.Close()

		if written, err := io.Copy(w, reader); err != nil {
			errchan <- fmt.Errorf("error writing %s (%d bytes written): %v", filename, written, err)
			return
		}

		if err := mwriter.Close(); err != nil {
			errchan <- err
			return
		}
	}()

	if client == nil {
		client = http.DefaultClient
	}
	resp, err := client.Do(req)
	merr := <-errchan

	if err != nil || merr != nil {
		return resp, fmt.Errorf("http error: %v, multipart error: %v", err, merr)
	}

	return resp, nil
}
